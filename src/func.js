const getSum = (str1, str2) => {
  const check = (str) => {
    for (let char of str) {
      if (isNaN(char)) {
        return false;
      }
    }
    
    return true;
  }
  
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }
  
  if (!check(str1) || !check(str2)) {
    return false;
  }
  
  if (str1 === '') {
    return str2;
  }
  
  if (str2 === '') {
    return str1;
  }
  
  let str = '';
  for (let i = 0; i < str1.length; i++) {
    str += (parseInt(str1[i]) + parseInt(str2[i])).toString();
  }
  
  return str;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (let post of listOfPosts) {
    if (post.author === authorName) {
      posts++;
    }
    
    if (post.comments === undefined) {
      continue;
    }
    
    for (let comment of post.comments) {
      if (comment.author === authorName) {
        comments++;
      }
    }
  }
  
  return 'Post:' + posts + ',comments:' + comments;
};

const tickets = (people) => {
  let twentyFive = 0;
  let fifty = 0;
  for (let amount of people) {
    switch (amount) {
      case 25:
        twentyFive++;
        break;
      case 50:
        if (twentyFive === 0) {
          return 'NO';
        }
        twentyFive--;
        fifty++;
        break;
      case 100:
        if (fifty !== 0) {
          fifty--;
          if (twentyFive === 0) {
            return 'NO';
          }
          twentyFive--;
        } else {
          if (twentyFive < 2) {
            return 'NO';
          }
          twentyFive -= 2;
        }
        break;
    }
  }

  return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
